const path = require("path");
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: "./server.ts",
  output: {
    filename: "server.js",
    path: path.resolve(__dirname, "dist"),
  },
  mode:"production",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  target: 'node',
  externals: [nodeExternals()],
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },

  devtool: "inline-source-map"
};
