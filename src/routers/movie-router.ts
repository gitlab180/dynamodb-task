import { Router } from "express";
import moviesController from "../controllers/movies.controller";

export const router = Router();
router.get("/createtable", moviesController.createTable.bind(moviesController));
router.get("/", moviesController.getAll.bind(moviesController));
router.get("/:title", moviesController.findByText.bind(moviesController));
router.put("/", moviesController.update.bind(moviesController));
router.delete("/", moviesController.delete.bind(moviesController));
