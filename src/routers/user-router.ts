import { Router } from "express";
import userController from "../controllers/users.controller";

export const userRouter = Router();
userRouter.get("/createtable", userController.createTable.bind(userController));
userRouter.get("/login", userController.login.bind(userController));
userRouter.get("/createUser", userController.create.bind(userController));