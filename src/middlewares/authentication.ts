import { NextFunction, Request, Response } from "express";
import * as JWT from "jsonwebtoken";

export class Authentication {
  public static refreshTokens = new Map<string, string>();

  public static authenticate(
    request: Request,
    response: Response,
    next: NextFunction
  ) {
    try {
      if (request.url.toLowerCase() !== "/login") {
        const authCookie = request.headers.cookie;

        if (!authCookie) {
          return response.status(401).send("no authentication token");
        }

        // Extract both access token and refresh token from cookies
        const accessTokenCookie = authCookie
          .split(";")
          .find((cookie) => cookie.trim().startsWith("accessToken="));

        if (!accessTokenCookie) {
          return response.status(401).send("no authentication token");
        }

        const token = accessTokenCookie.split("=")[1];
        try {
          JWT.verify(token, "secret");
        } catch (error) {
          return response.status(401).send("invalid credentials");
        }
      }
      next();
    } catch (error) {
      return response.status(401).send("invalid credentials");
    }
  }
}
