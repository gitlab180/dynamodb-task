import AWS from "aws-sdk";
import { Movie } from "../model/movie";

export class MoviesService {
  private dynamoDb: AWS.DynamoDB;
  constructor() {
    AWS.config.update({
      region: "",
      accessKeyId: "",
      secretAccessKey: "",
    });
    this.dynamoDb = new AWS.DynamoDB({});

  }

  public async createMovie(data: Movie) {
    const params: AWS.DynamoDB.PutItemInput = {
      TableName: "Movies",
      Item: {
        title: { S: data.title },
        rottenTomatoScore: { N: data.rottenTomatoScore.toString() },
        rating: { NS: [...data.ratings.map((rating) => rating.toString())] },
      },
    };
    return new Promise((resolve, reject) => {
      this.dynamoDb.putItem(params, function (err, data) {
        if (err) {
          console.log(err)
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }
  public async getMovie(title: string): Promise<Movie | undefined> {
    const params: AWS.DynamoDB.GetItemInput = {
      TableName: "Movies",
      Key: {
        title: { S: title },
      },
    };
    return new Promise((resolve, reject) => {
      this.dynamoDb.getItem(params, function (err, data) {
        if (err) {
          console.error("Unable to find movie", err);
          reject(err);
        } else {
          console.log("Found movie", data.Item);
          resolve(
            data.Item
              ? (AWS.DynamoDB.Converter.unmarshall(data.Item) as Movie)
              : undefined
          );
        }
      });
    });
  }
  public async removeMovie(title: string) {
    const params = {
      TableName: "Movies",
      Key: {
        title: { S: title },
      },
    };

    return new Promise((resolve, reject) => {
      this.dynamoDb.deleteItem(params, function (err) {
        if (err) {
          reject(err);
        } else {
          resolve(true);
        }
      });
    });
  }

  public async updateMovieScore(
    title: string,
    rottenTomatoScore: number
  ): Promise<boolean> {
    const params = {
      TableName: "Movies",
      Item: {
        title: { S: title },
        rottenTomatoScore: { N: rottenTomatoScore.toString() },
      },
      ReturnConsumedCapacity: "TOTAL",
    };
    return new Promise((resolve, reject) => {
      this.dynamoDb.putItem(params, (err, data) => {
        if (err) {
          console.error("Unable to find movie", err);
          reject(err);
        } else {
          resolve(true);
        }
      });
    });
  }
  public async getAllMovies(): Promise<Movie[] | []> {
    const params = {
      TableName: "Movies",
    };
    return new Promise((resolve, reject) => {
      this.dynamoDb.scan(params, function (err, data) {
        if (err) {
          console.error("Unable to find movies", err);
          reject(err);
        } else {
          console.log(`Found ${data.Count} movies`);

          resolve(
            data.Items
              ? data.Items?.map(
                  (item) => AWS.DynamoDB.Converter.unmarshall(item) as Movie
                )
              : []
          );
        }
      });
    });
  }
  public async createTable(): Promise<AWS.DynamoDB.CreateTableOutput | string> {
    const params: AWS.DynamoDB.CreateTableInput = {
      TableName: "Movies",
      KeySchema: [
        { AttributeName: "title", KeyType: "HASH" },
        { AttributeName: "rottenTomatoScore", KeyType: "RANGE" },
      ],
      AttributeDefinitions: [
        { AttributeName: "title", AttributeType: "S" },
        { AttributeName: "rottenTomatoScore", AttributeType: "N" },
        { AttributeName: "rating", AttributeType: "N" },
        { AttributeName: "imageUrl", AttributeType: "S" },
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
      },
      GlobalSecondaryIndexes: [
        {
          IndexName: "Movie",
          KeySchema: [
            {
              AttributeName: "imageUrl",
              KeyType: "HASH",
            },
            {
              AttributeName: "rating",
              KeyType: "RANGE",
            },
          ],
          Projection: {
            ProjectionType: "ALL",
          },
          ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1,
          },
        },
      ],
    };

    if ((await this.checkIfTableExists(params.TableName)) !== true) {
      return new Promise((resolve, reject) => {
        this.dynamoDb.createTable(params, function (err, data) {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });
    }
    return "table already exists";
  }


  private async checkIfTableExists(
    tableName: string
  ): Promise<boolean | undefined> {
    return new Promise((resolve, reject) => {
      this.dynamoDb.listTables({}, (err, data) => {
        if (err) {
          console.error("Error listing tables:", err.message);
          reject(err);
        } else {
          resolve(data?.TableNames?.includes(tableName));
        }
      });
    });
  }


}
