import AWS from "aws-sdk";
import { User } from "../model/user";

export class UserService {
  private dynamoDb: AWS.DynamoDB;
  constructor() {
    AWS.config.update({
      region: "",
      accessKeyId: "",
      secretAccessKey: "",
    });
    this.dynamoDb = new AWS.DynamoDB({});
  }

  public async createUser(data: User) {
    const params: AWS.DynamoDB.PutItemInput = {
      TableName: "Users",
      Item: {
        name: { S: data.name },
        email: { S: data.email },
        password: { S: data.password },
      },
    };
    return new Promise((resolve, reject) => {
      this.dynamoDb.putItem(params, function (err, data) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }
  public async getUser(email: string): Promise<User | undefined> {
    const params: AWS.DynamoDB.GetItemInput = {
      TableName: "Users",
      Key: {
        email: { S: email },
      },
    };
    return new Promise((resolve, reject) => {
      this.dynamoDb.getItem(params, function (err, data) {
        if (err) {
          console.error("Unable to find uer", err);
          reject(err);
        } else {
          console.log("Found User", data.Item);
          resolve(
            data.Item
              ? (AWS.DynamoDB.Converter.unmarshall(data.Item) as User)
              : undefined
          );
        }
      });
    });
  }

  public async createTable(): Promise<AWS.DynamoDB.CreateTableOutput | string> {
    const params: AWS.DynamoDB.CreateTableInput = {
      TableName: "Users",
      KeySchema: [
        { AttributeName: "name", KeyType: "HASH" },
        { AttributeName: "email", KeyType: "RANGE" },
      ],
      AttributeDefinitions: [
        { AttributeName: "name", AttributeType: "S" },
        { AttributeName: "email", AttributeType: "S" },
        { AttributeName: "password", AttributeType: "S" },
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
      },
    };

    if ((await this.checkIfTableExists(params.TableName)) !== true) {
      return new Promise((resolve, reject) => {
        this.dynamoDb.createTable(params, function (err, data) {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });
    }
    return "table already exists";
  }

  private async checkIfTableExists(
    tableName: string
  ): Promise<boolean | undefined> {
    return new Promise((resolve, reject) => {
      this.dynamoDb.listTables({}, (err, data) => {
        if (err) {
          console.error("Error listing tables:", err.message);
          reject(err);
        } else {
          resolve(data?.TableNames?.includes(tableName));
        }
      });
    });
  }
}
