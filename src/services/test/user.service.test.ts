
import AWS from 'aws-sdk';
import { UserService } from '../user.service';
import { User } from '../../model/user';



jest.mock('aws-sdk', () => {
    return {
      config: {
        update: jest.fn(),
      },
      DynamoDB: jest.fn(),
    };
  });

describe('UserService', () => {
  let userService: UserService;

  beforeEach(() => {
    userService = new UserService();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('createUser method', async () => {

    const mockUserData: User = {
      name: 'Test User',
      email: 'test@example.com',
      password: 'password123',
    };

    AWS.DynamoDB.prototype.putItem = jest.fn().mockImplementationOnce((params, callback) => {
      callback(null, {});
    });

    const result = await userService.createUser(mockUserData);

 
    expect(result).toEqual({});
    expect(AWS.DynamoDB.prototype.putItem).toHaveBeenCalledWith(
      {
        TableName: 'Users',
        Item: {
          name: { S: mockUserData.name },
          email: { S: mockUserData.email },
          password: { S: mockUserData.password },
        },
      },
      expect.any(Function)
    );
  });
});
