import { Request, Response } from "express";
import { MoviesService } from "../services/movies.service";

class MoviesController  {
  public movieService: MoviesService;
  constructor() {
    this.movieService = new MoviesService();
  }

  public async createTable(
    _request: Request,
    response: Response
  ): Promise<void> {
    try {
      response.send(await this.movieService.createTable());
    } catch (error) {
      response.status(500).send(error);
    }
  }

  public async create(request: Request, response: Response): Promise<void> {
    try {
      response.send(await this.movieService.createMovie(request.body));
    } catch (error) {
      response.status(500).send(error);
    }
  }

  public async update(request: Request, response: Response): Promise<void> {
    try {
      response.send(
        await this.movieService.updateMovieScore(
          request.body.title,
          request.body.rottenTomatoScore
        )
      );
    } catch (error) {
      response.status(500).send(error);
    }
  }
  public async findByText(
    request: Request,
    response: Response
  ): Promise<void> {
    try {
      response.send(await this.movieService.getMovie(request.body));
    } catch (error) {
      response.status(500).send(error);
    }
  }
  public async delete(request: Request, response: Response): Promise<void> {
    try {
      response.send(await this.movieService.removeMovie(request.params.title));
    } catch (error) {
      response.status(500).send(error);
    }
  }
  public async getAll(_request: Request, response: Response): Promise<void> {
    try {
      response.send(await this.movieService.getAllMovies());
    } catch (error) {
      response.status(500).send(error);
    }
  }
}

export default new MoviesController();
