import { Request, Response } from "express";
import { UserService } from "../services/user.service";
import { sign } from "jsonwebtoken";

class UsersController {
  public userService: UserService;
  constructor() {
    this.userService = new UserService();
  }
  public async createTable(
    _request: Request,
    response: Response
  ): Promise<void> {
    try {
      response.send(await this.userService.createTable());
    } catch (error) {
      response.status(500).send(error);
    }
  }

  public async create(request: Request, response: Response): Promise<void> {
    try {
      response.send(await this.userService.createUser(request.body));
    } catch (error) {
      response.status(500).send(error);
    }
  }

  public async login(request: Request, response: Response): Promise<void> {
    try {
      const user = await this.userService.getUser(request.body.email);
      if (user?.password === request.body.password) {
        const token = sign({ user }, "secret", { expiresIn: "5m" });
        response.cookie("accessToken", token, {
          maxAge: 12 * 60 * 60,
          secure: false,
          httpOnly: true,
          sameSite: "lax",
        });
      }
      response.send();
    } catch (error) {
      response.status(500).send(error);
    }
  }
}

export default new UsersController();
