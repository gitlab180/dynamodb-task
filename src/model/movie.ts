export interface Movie {
  title: string;
  ratings: number[];
  rottenTomatoScore: number;
  imageUrl: string;
}
