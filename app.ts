import express, { Router } from "express";
import helmet from "express";
import dotenv from "dotenv";
import { router } from "./src/routers/movie-router";
import { userRouter } from "./src/routers/user-router";
import { Authentication } from "./src/middlewares/authentication";

export class Application {
  private readonly application = express();
  constructor() {
    dotenv.config();
    this.mountBodyParser();
    this.setHeaders();
    this.moutAuthMiddleware();
    this.mountRoutes();
  }

  public start(): void {
    const PORT = process.env.PORT || 9000;

    this.application.listen(PORT, () => {
      console.log(`server started on port ${PORT}`);
    });
  }

  private moutAuthMiddleware(): void {
    this.application.use(Authentication.authenticate)
  }
  private mountBodyParser(): void {
    this.application.use(express.json());
  }
  private mountRoutes(): void {
    this.application.use("/movies", router);
    this.application.use("/users", userRouter);
  }

  private setHeaders() {
    this.application.use(helmet());
    this.application.disable("x-powered-by");
  }
}
